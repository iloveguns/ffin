$(function () {
    // Аккордеоны
    $('.panel-title a').click(function () {
        $('.panel').removeClass('show');

        if (typeof $(this).attr('aria-expanded') == 'undefined' || $(this).attr('aria-expanded') == 'false') {
            $(this).parent().parent().parent().addClass('show');
        }
    });

    // Мобильное меню
    $('.js-mobile-menu').click(function () {
        $('.mobile-menu').toggleClass('active');
    });

    // Плавный скролл пунктов меню
    $("nav a").click(function () {
        $('.mobile-menu').removeClass('active');
        var _href = $(this).attr("href");
        $("html, body").animate({scrollTop: $(_href).offset().top + "px"});
        return false;
    });

    // Подробнее в баннере
    $('.js-banner-text-btn').click(function () {
        let bannerTextContainer = $('.js-banner-text-container');
        let bannerTextBlock = $('.js-banner-text');
        let videoBlock = $('.video');
        let bannerBlock = $('.banner');

        if (bannerTextBlock.hasClass('show')) {
            $(this).attr("href", '#banner');
            $(this).find('span').text('Подробнее');
            $(this).find('img').css('transform', 'rotate(0)');
            bannerTextBlock.removeClass('show');
            bannerTextContainer.removeClass('show');
            videoBlock.removeClass('show');
            bannerBlock.removeClass('show');
        } else {
            $(this).attr("href", '#js-banner-text-anchor');
            $(this).find('span').text('Скрыть');
            $(this).find('img').css('transform', 'rotate(180deg)');
            bannerTextBlock.addClass('show');
            bannerTextContainer.addClass('show');
            videoBlock.addClass('show');
            bannerBlock.addClass('show');
        }

        var _href = $(this).attr("href");
        $("html, body").animate({scrollTop: $(_href).offset().top + "px"});

        return false;
    });

    // Подробнее в нотах
    $('.js-note-text-btn').click(function () {
        let ulBlock = $(this).parent().parent().find('.text-inner');

        if (ulBlock.hasClass('show')) {
            $(this).find('span').text('Подробнее');
            $(this).find('img').css('transform', 'rotate(0)');
            ulBlock.removeClass('show');
        } else {
            $(this).find('span').text('Скрыть');
            $(this).find('img').css('transform', 'rotate(180deg)');
            ulBlock.addClass('show');
        }

        $("html, body").animate({scrollTop: ulBlock.offset().top + "px"});

        return false;
    });

    // Подробнее в "О компании"
    $('.js-about-text-btn').click(function () {
        let countsBlock = $('.js-about-result-list');

        if (countsBlock.hasClass('show')) {
            $(this).find('span').text('Подробнее');
            $(this).find('img').css('transform', 'rotate(0)');
            countsBlock.removeClass('show');
        } else {
            $(this).find('span').text('Скрыть');
            $(this).find('img').css('transform', 'rotate(180deg)');
            countsBlock.addClass('show');
        }

        $("html, body").animate({scrollTop: $('.js-about-list').offset().top + "px"});

        return false;
    });
});

// Canvas-графики
$(function () {
    var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
        navigator.userAgent &&
        navigator.userAgent.indexOf('CriOS') == -1 &&
        navigator.userAgent.indexOf('FxiOS') == -1;

    if (isSafari) {
        // $('.mt-hide').hide();
        $('.offers__item__content__text .text-inner').addClass('safari-after');
    }

    var colorSet = [
        "#0681d1",
        "#1a2f44",
        "#164162",
        "#115280",
        "#0c6aa8",
    ];

    if ($(window).width() > 1024) {
        colorSet.splice(1, 0, "#ffffff");
    }

    CanvasJS.addColorSet("blue", colorSet);

    let chartConfig = {
        animationEnabled: true,
        backgroundColor: '#1d232f',
        colorSet: "blue",
        theme: "dark2",
        toolTip: {enabled: true, animationEnabled: true},

        // width: 285,
        // height: 285
    };


    let chartDataConfig = {
        // indexLabelPlacement: "inside",
        type: "doughnut",
        startAngle: 60,
        cursor: "pointer",
        innerRadius: 30,
        indexLabelFontSize: 12,
        indexLabel: "{label} #percent%",
        toolTipContent: "{fulllabel}",
        explodeOnClick: false,
        indexLabelLineColor: "transparent",
    };

    if ($(window).width() < 1024) {
        chartDataConfig.indexLabelPlacement = 'inside';
    }

    if ($(window).width() < 1024 || isSafari) {
        // chartActive
        let chartActive = new CanvasJS.Chart("chartActive", Object.assign(chartConfig, {
            data: [Object.assign(chartDataConfig, {
                dataPoints: [
                    {x: 123, y: 15, label: "BKE", fulllabel: "The Buckle, Inc.", exploded: true, modal: '#BKE_Modal'},
                    {y: 45, label: "ДС", fulllabel: 'Денежные средства', exploded: true, modal: '#ДС_Modal'},
                    {y: 5, label: "CAI", fulllabel: 'CAI international', exploded: true, modal: '#CAI_Modal'},
                    {y: 10, label: "SNAP", fulllabel: 'Snapchat Inc.', exploded: true, modal: '#SNAP_Modal'},
                    {
                        y: 10,
                        label: "BITA",
                        fulllabel: 'BITA – Bitauto Holdings (ADR)',
                        exploded: true,
                        modal: '#BITA_Modal'
                    },
                    {
                        y: 15,
                        label: "MBT",
                        fulllabel: 'Мобильные Телесистемы (MTC ADR)',
                        exploded: true,
                        modal: '#MBT_Modal'
                    },
                ],
                click: function (e) {
                    $(e.dataPoint.modal).modal('show');
                },
            })]
        }));
        chartActive.render();

        // chartBio
        let chartBio = new CanvasJS.Chart("chartBio", Object.assign(chartConfig, {
            data: [Object.assign(chartDataConfig, {
                dataPoints: [
                    {y: 25, label: "GILD", fulllabel: "Gilead Sciences", exploded: true, modal: '#GILD_Modal'},
                    {y: 25, label: "MRK", fulllabel: "Merck Inc.", exploded: true, modal: '#MRK_Modal'},
                    {
                        y: 20,
                        label: "ICPT",
                        fulllabel: "Intercept Pharmaceuticals",
                        exploded: true,
                        modal: '#ICPT_Modal'
                    },
                    {y: 20, label: "MYOK", fulllabel: "MyoKardia Inc.", exploded: true, modal: '#MYOK_Modal'},
                    {y: 10, label: "CDTX", fulllabel: "Cidara Therapeutics", exploded: true, modal: '#CDTX_Modal'},
                ],
                click: function (e) {
                    $(e.dataPoint.modal).modal('show');
                },
            })]
        }));
        chartBio.render();

        // chartDividend
        let chartDividend = new CanvasJS.Chart("chartDividend", Object.assign(chartConfig, {
            data: [Object.assign(chartDataConfig, {
                dataPoints: [
                    {y: 20, label: "BKE", fulllabel: 'The Buckle, Inc.', exploded: true, modal: '#BKE_Modal'},
                    {y: 20, label: "WELL", fulllabel: 'Welltower Inc.', exploded: true, modal: '#WELL_Modal'},
                    {
                        y: 20,
                        label: "IRT",
                        fulllabel: 'Independence Realty Trust, Inc.',
                        exploded: true,
                        modal: '#IRT_Modal'
                    },
                    {y: 20, label: "VGR", fulllabel: 'Vector Group Ltd.', exploded: true, modal: '#VGR_Modal'},
                    {y: 20, label: "MBT", fulllabel: 'Mobile TeleSystems', exploded: true, modal: '#MBT_Modal'},
                ],
                click: function (e) {
                    $(e.dataPoint.modal).modal('show');
                },
            })]
        }));
        chartDividend.render();

        // chartGrow
        let chartGrow = new CanvasJS.Chart("chartGrow", Object.assign(chartConfig, {
            data: [Object.assign(chartDataConfig, {
                dataPoints: [
                    {y: 10, label: "BB", fulllabel: "BlackBerry Ltd", exploded: true, modal: '#BB_Modal'},
                    {y: 10, label: "INTC", fulllabel: "Intel Corporation", exploded: true, modal: '#INTC_Modal'},
                    {y: 10, label: "ADBE", fulllabel: "Adobe Systems", exploded: true, modal: '#ADBE_Modal'},
                    {y: 10, label: "INTU", fulllabel: "Intuit Inc.", exploded: true, modal: '#INTU_Modal'},
                    {y: 10, label: "RAVN", fulllabel: "Raven Industries, Inc.", exploded: true, modal: '#RAVN_Modal'},
                    {y: 10, label: "MSFT", fulllabel: "Microsoft Corporation", exploded: true, modal: '#MSFT_Modal'},
                    {y: 10, label: "QADA", fulllabel: "QAD Inc.", exploded: true, modal: '#QADA_Modal'},
                    {y: 10, label: "BEAT", fulllabel: "BioTelemetry Inc", exploded: true, modal: '#BEAT_Modal'},
                    {y: 10, label: "CACI", fulllabel: "CACI INTERNATIONAL INC", exploded: true, modal: '#CACI_Modal'},
                    {y: 10, label: "MA", fulllabel: "Mastercard Inc", exploded: true, modal: '#MA_Modal'},
                ],
                click: function (e) {
                    $(e.dataPoint.modal).modal('show');
                },
            })]
        }));
        chartGrow.render();

        // chartPast
        let chartPast = new CanvasJS.Chart("chartPast", Object.assign(chartConfig, {
            data: [Object.assign(chartDataConfig, {
                dataPoints: [
                    {y: 20, label: "JCP", fulllabel: 'JC Penney', exploded: true, modal: '#JCP_Modal'},
                    {y: 20, label: "KHC", fulllabel: 'Kraft Heinz Co', exploded: true, modal: '#KHC_Modal'},
                    {y: 20, label: "GE", fulllabel: 'General Electric', exploded: true, modal: '#GE_Modal'},
                    {y: 20, label: "DB", fulllabel: 'Deutsche Bank', exploded: true, modal: '#DB_Modal'},
                    {y: 20, label: "SNAP", fulllabel: 'Snap Inc.', exploded: true, modal: '#SNAP_Modal'},
                ],
                click: function (e) {
                    $(e.dataPoint.modal).modal('show');
                },
            })]
        }));
        chartPast.render();

        // chartRetail
        let chartRetail = new CanvasJS.Chart("chartRetail", Object.assign(chartConfig, {
            data: [Object.assign(chartDataConfig, {
                dataPoints: [
                    {y: 25, label: "AEO", fulllabel: 'American Eagle Outfitters', exploded: true, modal: '#AEO_Modal'},
                    {y: 25, label: "BKE", fulllabel: 'Buckle Inc.', exploded: true, modal: '#BKE_Modal'},
                    {y: 25, label: "DDS", fulllabel: 'Dillard’s', exploded: true, modal: '#DDS_Modal'},
                    {y: 25, label: "GPS", fulllabel: 'GAP Inc.', exploded: true, modal: '#GPS_Modal'},
                ],
                click: function (e) {
                    $(e.dataPoint.modal).modal('show');
                },
            })]
        }));
        chartRetail.render();
    } else {
        $(document).scroll(function () {
            let s_top = document.documentElement.scrollTop;

            // chartActive
            var chartObject = $('#chartActive');
            if (s_top > chartObject.offset().top - 500) {
                if (chartObject.height() == 0) {
                    chartObject.height(400);

                    let chartActive = new CanvasJS.Chart("chartActive", Object.assign(chartConfig, {
                        data: [Object.assign(chartDataConfig, {
                            dataPoints: [
                                {
                                    x: 123,
                                    y: 15,
                                    label: "BKE",
                                    fulllabel: "The Buckle, Inc.",
                                    exploded: true,
                                    modal: '#BKE_Modal'
                                },
                                {
                                    y: 45,
                                    label: "ДС",
                                    fulllabel: 'Денежные средства',
                                    exploded: true,
                                    modal: '#ДС_Modal'
                                },
                                {
                                    y: 5,
                                    label: "CAI",
                                    fulllabel: 'CAI international',
                                    exploded: true,
                                    modal: '#CAI_Modal'
                                },
                                {
                                    y: 10,
                                    label: "SNAP",
                                    fulllabel: 'Snapchat Inc.',
                                    exploded: true,
                                    modal: '#SNAP_Modal'
                                },
                                {
                                    y: 10,
                                    label: "BITA",
                                    fulllabel: 'BITA – Bitauto Holdings (ADR)',
                                    exploded: true,
                                    modal: '#BITA_Modal'
                                },
                                {
                                    y: 15,
                                    label: "MBT",
                                    fulllabel: 'Мобильные Телесистемы (MTC ADR)',
                                    exploded: true,
                                    modal: '#MBT_Modal'
                                },
                            ],
                            click: function (e) {
                                $(e.dataPoint.modal).modal('show');
                            },
                        })]
                    }));
                    chartActive.render();
                }
            }

            // chartBio
            var chartObject = $('#chartBio');
            if (s_top > chartObject.offset().top - 500) {
                if (chartObject.height() == 0) {
                    chartObject.height(400);

                    let chartBio = new CanvasJS.Chart("chartBio", Object.assign(chartConfig, {
                        data: [Object.assign(chartDataConfig, {
                            dataPoints: [
                                {
                                    y: 25,
                                    label: "GILD",
                                    fulllabel: "Gilead Sciences",
                                    exploded: true,
                                    modal: '#GILD_Modal'
                                },
                                {y: 25, label: "MRK", fulllabel: "Merck Inc.", exploded: true, modal: '#MRK_Modal'},
                                {
                                    y: 20,
                                    label: "ICPT",
                                    fulllabel: "Intercept Pharmaceuticals",
                                    exploded: true,
                                    modal: '#ICPT_Modal'
                                },
                                {
                                    y: 20,
                                    label: "MYOK",
                                    fulllabel: "MyoKardia Inc.",
                                    exploded: true,
                                    modal: '#MYOK_Modal'
                                },
                                {
                                    y: 10,
                                    label: "CDTX",
                                    fulllabel: "Cidara Therapeutics",
                                    exploded: true,
                                    modal: '#CDTX_Modal'
                                },
                            ],
                            click: function (e) {
                                $(e.dataPoint.modal).modal('show');
                            },
                        })]
                    }));
                    chartBio.render();
                }
            }

            // chartDividend
            var chartObject = $('#chartDividend');
            if (s_top > chartObject.offset().top - 500) {
                if (chartObject.height() == 0) {
                    chartObject.height(400);

                    let chartDividend = new CanvasJS.Chart("chartDividend", Object.assign(chartConfig, {
                        data: [Object.assign(chartDataConfig, {
                            dataPoints: [
                                {
                                    y: 20,
                                    label: "BKE",
                                    fulllabel: 'The Buckle, Inc.',
                                    exploded: true,
                                    modal: '#BKE_Modal'
                                },
                                {
                                    y: 20,
                                    label: "WELL",
                                    fulllabel: 'Welltower Inc.',
                                    exploded: true,
                                    modal: '#WELL_Modal'
                                },
                                {
                                    y: 20,
                                    label: "IRT",
                                    fulllabel: 'Independence Realty Trust, Inc.',
                                    exploded: true,
                                    modal: '#IRT_Modal'
                                },
                                {
                                    y: 20,
                                    label: "VGR",
                                    fulllabel: 'Vector Group Ltd.',
                                    exploded: true,
                                    modal: '#VGR_Modal'
                                },
                                {
                                    y: 20,
                                    label: "MBT",
                                    fulllabel: 'Mobile TeleSystems',
                                    exploded: true,
                                    modal: '#MBT_Modal'
                                },
                            ],
                            click: function (e) {
                                $(e.dataPoint.modal).modal('show');
                            },
                        })]
                    }));
                    chartDividend.render();
                }
            }

            // chartGrow
            var chartObject = $('#chartGrow');
            if (s_top > chartObject.offset().top - 500) {
                if (chartObject.height() == 0) {
                    chartObject.height(400);

                    let chartGrow = new CanvasJS.Chart("chartGrow", Object.assign(chartConfig, {
                        data: [Object.assign(chartDataConfig, {
                            dataPoints: [
                                {y: 10, label: "BB", fulllabel: "BlackBerry Ltd", exploded: true, modal: '#BB_Modal'},
                                {
                                    y: 10,
                                    label: "INTC",
                                    fulllabel: "Intel Corporation",
                                    exploded: true,
                                    modal: '#INTC_Modal'
                                },
                                {
                                    y: 10,
                                    label: "ADBE",
                                    fulllabel: "Adobe Systems",
                                    exploded: true,
                                    modal: '#ADBE_Modal'
                                },
                                {y: 10, label: "INTU", fulllabel: "Intuit Inc.", exploded: true, modal: '#INTU_Modal'},
                                {
                                    y: 10,
                                    label: "RAVN",
                                    fulllabel: "Raven Industries, Inc.",
                                    exploded: true,
                                    modal: '#RAVN_Modal'
                                },
                                {
                                    y: 10,
                                    label: "MSFT",
                                    fulllabel: "Microsoft Corporation",
                                    exploded: true,
                                    modal: '#MSFT_Modal'
                                },
                                {y: 10, label: "QADA", fulllabel: "QAD Inc.", exploded: true, modal: '#QADA_Modal'},
                                {
                                    y: 10,
                                    label: "BEAT",
                                    fulllabel: "BioTelemetry Inc",
                                    exploded: true,
                                    modal: '#BEAT_Modal'
                                },
                                {
                                    y: 10,
                                    label: "CACI",
                                    fulllabel: "CACI INTERNATIONAL INC",
                                    exploded: true,
                                    modal: '#CACI_Modal'
                                },
                                {y: 10, label: "MA", fulllabel: "Mastercard Inc", exploded: true, modal: '#MA_Modal'},
                            ],
                            click: function (e) {
                                $(e.dataPoint.modal).modal('show');
                            },
                        })]
                    }));
                    chartGrow.render();
                }
            }

            // chartPast
            var chartObject = $('#chartPast');
            if (s_top > chartObject.offset().top - 500) {
                if (chartObject.height() == 0) {
                    chartObject.height(400);

                    let chartPast = new CanvasJS.Chart("chartPast", Object.assign(chartConfig, {
                        data: [Object.assign(chartDataConfig, {
                            dataPoints: [
                                {y: 20, label: "JCP", fulllabel: 'JC Penney', exploded: true, modal: '#JCP_Modal'},
                                {y: 20, label: "KHC", fulllabel: 'Kraft Heinz Co', exploded: true, modal: '#KHC_Modal'},
                                {y: 20, label: "GE", fulllabel: 'General Electric', exploded: true, modal: '#GE_Modal'},
                                {y: 20, label: "DB", fulllabel: 'Deutsche Bank', exploded: true, modal: '#DB_Modal'},
                                {y: 20, label: "SNAP", fulllabel: 'Snap Inc.', exploded: true, modal: '#SNAP_Modal'},
                            ],
                            click: function (e) {
                                $(e.dataPoint.modal).modal('show');
                            },
                        })]
                    }));
                    chartPast.render();
                }
            }

            // chartRetail
            var chartObject = $('#chartRetail');
            if (s_top > chartObject.offset().top - 500) {
                if (chartObject.height() == 0) {
                    chartObject.height(400);

                    let chartRetail = new CanvasJS.Chart("chartRetail", Object.assign(chartConfig, {
                        data: [Object.assign(chartDataConfig, {
                            dataPoints: [
                                {
                                    y: 25,
                                    label: "AEO",
                                    fulllabel: 'American Eagle Outfitters',
                                    exploded: true,
                                    modal: '#AEO_Modal'
                                },
                                {y: 25, label: "BKE", fulllabel: 'Buckle Inc.', exploded: true, modal: '#BKE_Modal'},
                                {y: 25, label: "DDS", fulllabel: 'Dillard’s', exploded: true, modal: '#DDS_Modal'},
                                {y: 25, label: "GPS", fulllabel: 'GAP Inc.', exploded: true, modal: '#GPS_Modal'},
                            ],
                            click: function (e) {
                                $(e.dataPoint.modal).modal('show');
                            },
                        })]
                    }));
                    chartRetail.render();
                }
            }
        });
    }

    // Sorting
    $('.js-sort-btn').on("click", function (e) {
        var $this = $(this);
        var $list = $('.js-sort-list');
        var $productList = $('.offers__item', $list);

        var order = $this.hasClass('asc');

        $productList.sort(function (a, b) {
            if ($this.hasClass('amount')) {
                var keyA = $(a).attr("data-amount");
                var keyB = $(b).attr("data-amount");
            } else {
                var keyA = $(a).attr("data-goal");
                var keyB = $(b).attr("data-goal");
            }

            return order ? (parseInt(keyA) - parseInt(keyB)) : (parseInt(keyB) - parseInt(keyA));
        });

        $.each($productList, function (index, row) {
            $list.append(row);
        });

        $this.toggleClass('asc');
        $('.js-sort-btn').removeClass('active');
        $this.addClass('active');
        e.preventDefault();
    });

    $('.js-sort-btn.amount').click();
    $('.js-sort-btn.amount').click();
});



