<?php
if (!empty($_POST)) {
    $sendSuccess = true;

    // DB
    /*$host = 'thor.ffin.ru';
    $database = 'db_dun';
    $user = 'ffin_site';
    $password = 'site_2018_ffin_483';

    $mysqli = new mysqli($host, $user, $password, $database);
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    // data
    $lastName = '';
    $name = $_POST['name'];
    $secondName = '';
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $text = $_POST['text'];

    if (!$mysqli->query('CALL db_dun.u_in("' . $lastName . '", "' . $name . '", "' . $secondName . '", "' . $phone . '", "' . $email . '","dn"."' . $text . '")')) {
        echo "Не удалось вызвать хранимую процедуру: (" . $mysqli->errno . ") " . $mysqli->error;
    }else{
        $sendSuccess = true;
    }*/
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Каждый портфель ноты по своей сути это фонд, состоящий из ценных бумаг и денежных
                    средств. Перечисляя деньги за покупку нот, вы, в качестве инвестора, пополняете фонд и
                    становитесь его совладельцем.">
    <meta name="keyword"
          content="Ноты Фридом Финанс, Freedom Finance Notes, Freedom Biotech, Фридом Биотех, Freedom Active, Фридом Актив, Freedom Dividend, Фридом Дивиденды">
    <title>Freedom Finance | Фридом Финанс</title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">

    <link rel="stylesheet" href="css/style.css">

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<div class="bg-lines"></div>

<div class="container" style="position:relative;">
    <div class="video">
        <div class="over"></div>
        <video width="100%" height="auto" autoplay muted loop playsinline>
            <source src="media/video.mov"/>
        </video>
    </div>
</div>

<div class="mobile-header">
    <a href="#" class="burger js-mobile-menu"><img src="img/burger.png" alt=""></a>
    <img class="logo" src="img/logo.png" alt="">
    <a href="tel:+35722282923" class="phone"><img src="img/phone.png" alt=""></a>
</div>

<div id="nav" class="navigation mobile-menu">
    <div class="navigation__inner">
        <img src="img/logo.png" class="logo" alt="">
        <a class="close js-mobile-menu" href="#"><img src="img/mobile-close.png" alt=""></a>
        <br/>
        <a class="phone" href="tel:+35722282923">+ 357 22 28 29 23 <img src="img/phone.png" alt=""></a>
        <nav>
            <ul>
                <li><a href="#banner">О нотах</a></li>
                <li><a href="#offers">Предложения по нотам</a>
                    <ul>
                        <li><a href="#noteActive">freedom active</a></li>
                        <li><a href="#noteGrow">акции роста</a></li>
                        <li><a href="#noteDividend">freedom devidend</a></li>
                        <li><a href="#noteBio">freedom biotech</a></li>
                        <li><a href="#notePast">былое величие</a></li>
                        <li><a href="#noteRetail">freedom retail</a></li>
                    </ul>
                </li>
                <li><a href="#about">О Фридом Финанс</a></li>
                <li><a href="#contacts">Контакты</a></li>
            </ul>
        </nav>
        <div class="site">
            <a href="http://ffin.biz" target="_blank"><img src="img/globe.png" alt=""> ffin.biz</a>
        </div>
    </div>
</div>

<header class="m-hide">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="img/logo.png" alt="">
            </div>
            <div class="col-md-7 col-md-offset-2">
                <div class="contacts">
                    <span class="call">Звоните нам — <a href="tel:+35722282923">+ 357 22 28 29 23</a>. Мы ответим на все вопросы</span>
                    <a class="site" href="http://ffin.bz" target="_blank"><img src="img/globe.png"
                                                                               alt="">ffin.bz</a>
                </div>
                <nav>
                    <ul>
                        <li><a href="#banner">О нотах</a></li>
                        <li><a href="#offers">Предложения по нотам</a></li>
                        <li><a href="#about">О Фридом Финанс</a></li>
                        <li><a href="#contacts">Контакты</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<div id="banner" class="banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>НОТЫ FREEDOM —<br/>
                    <span>защита капитала<br/>
                    и высокий доход</span>
                </h1>
                <div class="square"></div>
            </div>
        </div>

        <div id="js-banner-text-anchor" class="text-container js-banner-text-container">
            <div class="text js-banner-text">
                <div class="bg-dark">
                    <p class="lead">Нота – это финансовый инструмент, стоимость которого привязана к изменению котировок
                        определенного набора ценных бумаг.</p>

                    <div class="text-hidden">
                        <p>По сути, нота соответствует доле в фонде, который управляет
                            деньгами инвесторов по разработанной экспертами «Фридом Финанс» стратегии. При этом вам не
                            придется беспокоиться о безопасности инвестиций и следить за рынками: наличие фонда
                            позволяет
                            сбалансировать риски, а всю оперативную работу за вас сделают трейдеры «Фридом Финанс».</p>
                    </div>

                    <p>Инвестирование в ценные бумаги денежных средств портфеля
                        ноты, в
                        свою очередь, происходит в строгом соответствии с принципами, заложенными в стратегию и
                        структуру конкретного портфеля нот.</p>

                    <p>Поскольку, каждый портфель ноты имеет собственную структуру из ценных бумаг с определенными
                        характеристиками, сами ноты, которые являются долей такого портфеля, именуются
                        «структурными». </p>
                </div>

                <div class="panel-group" id="accordion">
                    <!--                    <div class="row">-->
                    <!--                        <div class="col-md-6">-->
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    О составе ноты в разные периоды времени
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>В разные периоды времени фонд может содержать различную долю денежных средств,
                                    которая образуется на следующих временных отрезках:</p>
                                <ul>
                                    <li>между приходом новых денег в портфель ноты и покупкой ценных бумаг;</li>
                                    <li>между продажей и покупкой бумаг в портфель;</li>
                                    <li>между принятием заявки на погашение нот и перечислением за них денежных
                                        средств
                                        инвестору и т.д.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    О минимизации рисков и росте капитала
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Нота «Фридом Финанс» – это структурный инвестиционный продукт, в котором заложены
                                    меры для минимизации рисков и роста капитала.</p>
                                <p>Минимизация рисков обеспечивается:</p>
                                <ul>
                                    <li>широкой диверсификацией (включением в портфель значительного количества акций
                                        финансово надежных, устойчивых компаний);
                                    </li>
                                    <li>постоянным наблюдением и изучением экспертами «Фридом Финанс» рыночной
                                        конъюнктуры, немедленным реагированием для защиты инвестиций рисков или для
                                        максимизации прибыли.
                                    </li>
                                </ul>
                                <p>Рост инвестированного капитала в таком продукте происходит за счёт роста котировок
                                    входящих в портфель фонда бумаг, а также выплаты дивидендов по этим бумагам.</p>
                                <p>Несмотря на меры по минимизации рисков, нота, как и любая инвестиция на фондовом
                                    рынке с высокой целевой доходностью, может подвергаться рискам, таким как риск
                                    эмитентов бумаг, к которым привязана нота, макроэкономический риск.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    О диверсификации
                                </a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Ноты «Фридом Финанс» позволяют объединить средства инвесторов и распределить их
                                    равномерно по различным классам активов (диверсификация). Даже небольшие по объему
                                    инвестиции (большинство нот стоит $1000) будут защищены от непредсказуемых изменений
                                    в цене отдельных ценных бумаг.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    О марже, комиссиях
                                </a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Структурные ноты «Фридом Финанс» являются маржинальными и могут служить в качестве
                                    обеспечения покупки других бумаг на тот же брокерский счёт в пределах 50% стоимости
                                    нот.</p>
                                <p>Комиссия при покупке ноты составляет 1,5%.</p>
                                <p>Вознаграждение компании за успешное инвестирование и, как следствие, увеличение
                                    инвестированного капитала клиента составляет 25% от прибыли. Оно рассчитывается и
                                    уплачивается один раз по итогу календарного квартала. Причем, в расчете прибыли
                                    используется показатель high watermark - высшая стоимость ноты на конец квартала
                                    либо на день покупки нот клиентом. Например, по итогу I квартала зафиксирован рост
                                    ноты, по итогу II квартала – снижение, по итогу III квартала - рост, но цена ноты в
                                    III квартале не превысила цену 1-го квартала. Вознаграждение компании за III квартал
                                    в этом случае не взимается.</p>
                                <p>Таким образом, компания получает вознаграждение только при условии устойчивого роста
                                    капитала клиента.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                    О погашении нот
                                </a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Отправить заявку на погашение нот можно в любой момент. Погашение нот «Фридом Финанс»
                                    происходит без комиссии и потери дохода для инвестора в течении пяти дней от даты
                                    заявки.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="button js-banner-text-btn" href="#"><span>Подробнее</span> <img src="img/arrow.png"
                                                                                      alt=""></a>
        </div>
    </div>
</div>

<div id="person" class="person">
    <div class="container person__container">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <div class="person__about">
                    <div class="person__about__line"></div>
                    <img src="img/egor.png" alt="" id="egor">
                    <h2>Егор Романюк</h2>
                    <p>Заместитель Генерального Директора
                        по Управлению Активами Freedom Finance</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-1">
                <div class="person__text js-person-text-container">
                    <p class="lead">На американском фондовом рынке с 2005-го года. Профессиональный управляющий активами
                        с
                        2008-го года.</p>

                    <p>Автор финансовых статей Forbes, Seeking Alpha и Investopedia. Лидер статистики <a
                                href="http://invest-idei.ru" target="_blank">invest-idei.ru</a> с августа
                        2017-го
                        года. Спикер – Опционные конференции. Спикер – клуб инвесторов Фридом Финанс.</p>

                    <p>Специализация: Special Events (экстраординарные ситуации). Исключительно Американский рынок
                        акций, опционы.</p>

                    <h3>Результаты управления</h3>
                    <div class="person__result-list">
                        <div class="person__result-item">
                            2014<br/><span>+174%</span>
                        </div>
                        <div class="person__result-item">
                            2015<br/><span>+36%</span>
                        </div>
                        <div class="person__result-item">
                            2016<br/><span>+31%</span>
                        </div>
                        <div class="person__result-item">
                            2017<br/><span>+89%</span>
                        </div>
                        <div class="person__result-item">
                            2018<br/><span>+114%</span>
                        </div>
                    </div>
                    <p>Максимальная просадка портфелей -18% (первая половина 2017-го года).</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="offers" class="offers__header">
    <div class="container">
        <h2>Предложения <br/><span>по нотам</span></h2>

        <div class="sort-block">
            <p>Сортировать предложения</p>
            <a class="button no-active js-sort-btn amount asc" href="#">
                Минимальная сумма входа <img src="img/sort.png" alt="">
            </a>
            <a class="button no-active js-sort-btn goal asc" href="#">
                Целевая доходность <img src="img/sort.png" alt="">
            </a>
        </div>
    </div>
</div>

<!-- Offers items -->
<div class="container js-sort-list">
    <div id="noteDividend" class="offers__item" data-amount="1000" data-goal="30">
        <div class="offers__item__header">
            <div class="offers__item__header__line"></div>
            <div class="offers__item__header__img">
                <img src="img/note-bg/dividend.png" alt="">
            </div>
            <div class="offers__item__header__name">
                <span>Нота</span><br/>
                Freedom<br/>Dividend
                <a class="pdf-download" href="pdf/dividend.pdf" download><img src="img/pdf.png" alt=""></a>
            </div>
            <div class="offers__item__header__counts">
                <div class="offers__item__header__counts__item">
                    <p>Минимальная<br/>сумма входа</p>
                    <p class="count">$1000</p>
                </div>
                <div class="offers__item__header__counts__item">
                    <div class="offers__item__header__dark-line"></div>
                    <p>Целевая<br/>доходность</p>
                    <p class="count">30%</p>
                </div>
            </div>
        </div>
        <div class="offers__item__content">

            <div class="offers__item__content__chart">
                <div class="chart-lines"><img src="img/pie_lines.png" alt=""></div>
                <div id="chartDividend"></div>
            </div>
            <div class="offers__item__content__text">
                <div class="text-inner">
                    <h3>Описание</h3>
                    <ul>
                        <li>
                            <p>В данный инвестиционный продукт включены эмитенты, стабильно выплачивающие дивиденды
                                на
                                протяжении нескольких десятков лет. Средняя дивидендная доходность активов в
                                портфеле
                                за
                                2017 год без учета специальных дивидендов составила 7,15%, а с их учетом — 9,03%
                                годовых.
                            </p>
                        </li>
                        <span class="mt-hide">
                            <li>
                                <p>Преимущество продукта в том, что включенные в портфель компании демонстрируют не
                                    только
                                    потенциал роста, но и высокую сопротивляемость рецессии.</p>
                            </li>
                            <li>
                                <p>Результаты анализа компаний позволяют предположить, что в среднесрочной перспективе
                                    значительных изменений в их дивидендной политике не произойдет.</p>
                            </li>
                            <li>
                                <p>Средний показатель P/E (цена акции / прибыль на акцию) составил 23,56, что говорит о
                                    потенциале роста портфеля за счет цены входящих бумаг.</p>
                            </li>
                            <li>
                                <p>Средний показатель риска ноты составляет 0,7, что свидетельствует об
                                    умеренной волатильности портфеля.</p>
                            </li>
                        </span>
                    </ul>
                    <span class="mt-hide">
                        <h3>Стратегия</h3>
                        <p>Управляя Портфелем Ноты, Брокер имеет право внести изменения в указанных случаях и
                            следующим
                            образом:</p>
                        <ul>
                            <li>
                                <p>В случае сокращения величины дивидендных выплат по входящей в ноту ценной бумаге
                                    более
                                    чем на 10% по сравнению с аналогичным предыдущим периодом Управляющий вправе
                                    заменить
                                    бумагу или исключить ее, перераспределив денежные средства между осталь- ными
                                    компаниями, входящими в Ноту.
                                </p>
                            </li>
                            <li>
                                <p>В случае замены вместо выходящей бумаги приобретается ценная бумага с более высоким
                                    уровнем текущей дивидендной доходности (в процентах – Yield) и обладающая
                                    фундаментальным потенциалом роста.</p>
                            </li>
                            <li>
                                <p>При исключении ценной бумаги должно соблюдаться условие о наличии в портфеле Ноты
                                    не
                                    менее четырех ценных бумаг.</p>
                            </li>
                            <li><p>Один актив не может занимать более 33,3% портфеля. В случае возникновения дисбаланса
                                    активов Брокер обязан провести балансировку портфеля ноты в течение 30 дней.</p>
                            </li>
                        </ul>
                        <h3>Параметры Ноты</h3>
                        <table class="table">
                            <tr>
                                <td>Тикер Ноты</td>
                                <th>FN_DVD</th>
                            </tr>
                            <tr>
                                <td>Инвестиционная цель стратегии Ноты</td>
                                <th>25% годовых</th>
                            </tr>
                            <tr>
                                <td>Ставка вознаграждения Брокера</td>
                                <th>25% от положительного результата</th>
                            </tr>
                            <tr>
                                <td>Минимальная сумма для подачи заявки на приобретение Ноты</td>
                                <th>1000 USD</th>
                            </tr>
                            <tr>
                                <td>Комиссия за покупку Ноты</td>
                                <th>1,5%</th>
                            </tr>
                        </table>
                    </span>
                </div>
                <div class="buttons">
                    <a href="#" class="button no-active js-note-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                    <a href="#" data-toggle="modal" data-target="#buyModal" class="button">Хочу купить ноту!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="noteBio" class="offers__item" data-amount="1000" data-goal="30">
        <div class="offers__item__header">
            <div class="offers__item__header__line"></div>
            <div class="offers__item__header__img">
                <img src="img/note-bg/bio.png" alt="">
            </div>
            <div class="offers__item__header__name">
                <span>Нота</span><br/>
                Freedom<br/>Biotech
                <a class="pdf-download" href="pdf/bio.pdf" download><img src="img/pdf.png" alt=""></a>
            </div>
            <div class="offers__item__header__counts">
                <div class="offers__item__header__counts__item">
                    <p>Минимальная<br/>сумма входа</p>
                    <p class="count">$1000</p>
                </div>
                <div class="offers__item__header__counts__item">
                    <div class="offers__item__header__dark-line"></div>
                    <p>Целевая<br/>доходность</p>
                    <p class="count">30%</p>
                </div>
            </div>
        </div>
        <div class="offers__item__content">
            <div class="offers__item__content__chart">
                <div class="chart-lines"><img src="img/pie_lines.png" alt=""></div>
                <div id="chartBio"></div>
            </div>
            <div class="offers__item__content__text">
                <div class="text-inner">
                    <h3>Описание</h3>
                    <ul>
                        <li>
                            <p>Данная Нота состоит из компаний биотехнологического сектора.</p>
                        </li>
                        <li>
                            <p>Портфель разделен на две части. Первая состоит из надежных, зарекомендовавших себя на
                                рынке
                                компаний. Вторая – из новых перспективных проектов, которые, по нашим прогнозам,
                                близки к
                                прорыву в исследованиях тех или иных медицинских препаратов.</p>
                        </li>
                        <li>
                            <p>Были отобраны три компании, препараты которых проходят третью, финальную, фазу
                                исследований.
                                Положительный результат на этой фазе позволяет представить разработку на рассмотрение
                                регулятора, который должен принять решение о выдаче разрешения на продажу.
                                Инвестирование в
                                указанные компании носит спекулятивный характер в силу особенностей функционирования
                                биотехнологического сектора.</p>
                        </li>
                        <li>
                            <p>Для диверсификации риска в портфель были добавлены акции двух зарекомендовавших себя на
                                рынке
                                компаний, прибыль которых стабильно растет. Эти эмитенты предлагают большой
                                ассортимент
                                сертифицированных препаратов и ведут исследования в новых перспективных областях.</p>
                        </li>
                    </ul>
                    <h3>Стратегия</h3>
                    <p>Управляющий имеет право внести изменения в портфель Ноты в указанных случаях и следующим
                        образом.</p>
                    <p>В отношении новых компаний, близких к прорыву в исследованиях:</p>
                    <ul>
                        <li>
                            <p>В случае положительного результата исследований компаний, чьи разработки сейчас
                                находятся
                                на финальной стадии, произойдет быстрый рост акций. В этом случае Управляющий
                                может
                                закрыть позицию, зафиксировав прибыль, что будет означать реализацию инвестиционного
                                ожидания.</p>
                        </li>
                        <li>
                            <p>В случае отрицательного результата исследований на финальной стадии возможно резкое
                                падение
                                стоимости акций указанных эмитентов. В таком случае Управляющий вправе закрыть
                                позицию и
                                зафиксировать убыток.</p>
                        </li>
                        <li>
                            <p>По итогам закрытия позиции Управляющий имеет право включить новую ценную бумагу, выбрав
                                компанию из биотехнологического сектора, которая успешно завершила вторую фазу
                                исследований
                                и сейчас находится на третьей.</p>
                        </li>
                    </ul>
                    <p>В отношении крупных известных компаний:</p>
                    <ul>
                        <li>
                            <p>В случае роста акций голубых фишек данной Ноты (в настоящий момент это GILD, MRK) в
                                течение одного года Управляющий имеет право закрыть позицию, фиксируя прибыль, что
                                будет
                                означать реализацию инвестиционного ожидания.</p>
                        </li>
                        <li>
                            <p>По итогам закрытия позиции Управляющий имеет право включить в портфель новую ценную
                                бумагу
                                компании из биотехнологического сектора, имеющую рыночную капитализацию не менее $100
                                млрд.</p>
                        </li>
                    </ul>
                    <p>В отношении всех компаний Ноты:</p>
                    <ul>
                        <li>
                            <p>При исключении ценной бумаги должно соблюдаться условие, при котором одна ценная бумага
                                (лот
                                бумаг одного вида одного эмитента) не может занимать более 33,3% портфеля ноты.</p>
                        </li>
                    </ul>
                    <h3>Параметры Ноты</h3>
                    <table class="table">
                        <tr>
                            <td>Тикер Ноты</td>
                            <th>FN_BIO</th>
                        </tr>
                        <tr>
                            <td>Инвестиционная цель стратегии Ноты</td>
                            <th>40% годовых</th>
                        </tr>
                        <tr>
                            <td>Ставка вознаграждения Брокера</td>
                            <th>25% от положительного результата</th>
                        </tr>
                        <tr>
                            <td>Минимальная сумма для подачи заявки на приобретение Ноты</td>
                            <th>1000 USD</th>
                        </tr>
                        <tr>
                            <td>Комиссия за покупку Ноты</td>
                            <th>1,5%</th>
                        </tr>
                    </table>
                </div>
                <div class="buttons">
                    <a href="#" class="button no-active js-note-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                    <a href="#" data-toggle="modal" data-target="#buyModal" class="button">Хочу купить ноту!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="noteGrow" class="offers__item" data-amount="10000" data-goal="40">
        <div class="offers__item__header">
            <div class="offers__item__header__line"></div>
            <div class="offers__item__header__img">
                <img src="img/note-bg/grow.png" alt="">
            </div>
            <div class="offers__item__header__name">
                <span>Нота</span><br/>
                Акции<br/>роста
                <a class="pdf-download" href="pdf/grow.pdf" download><img src="img/pdf.png" alt=""></a>
            </div>
            <div class="offers__item__header__counts">
                <div class="offers__item__header__counts__item">
                    <p>Минимальная<br/>сумма входа</p>
                    <p class="count">$10 000</p>
                </div>
                <div class="offers__item__header__counts__item">
                    <div class="offers__item__header__dark-line"></div>
                    <p>Целевая<br/>доходность</p>
                    <p class="count">40%</p>
                </div>
            </div>
        </div>
        <div class="offers__item__content">

            <div class="offers__item__content__chart">
                <div class="chart-lines"><img src="img/pie_lines.png" alt=""></div>
                <div id="chartGrow"></div>
            </div>
            <div class="offers__item__content__text">
                <div class="text-inner">
                    <h3>Структура*</h3>
                    <ul>
                        <li>
                            <p>Любой актив ноты может занимать не более 10% портфеля. Допускается использование «плеча»
                                до 50%, либо наличие денежной позиции в случае отсутствия актуальных идей на момент
                                закрытия текущих позиций.
                            </p>
                        </li>
                        <span class="mt-hide">
                            <li>
                                <p>В портфель включены бумаги из технологического, медицинского, энергетического и
                                    логистического секторов.</p>
                            </li>
                            <li>
                                <p>Допускается наличие бумаг из других секторов в случае обнаружения каких-либо
                                    компаний,
                                    способных внедрять инновации, приводящие к радикальному повышению эффективности
                                    бизнеса.</p>
                            </li>
                        </span>
                    </ul>
                    <span class="mt-hide">
                        <h3>Стратегия</h3>
                        <ul>
                            <li>
                                <p>Управляющий отслеживает упоминаемость публичных компаний растущих секторов
                                    экономики в целевых СМИ США, фиксируя акции с оптимальным количеством упоминаний на
                                    единицу времени.
                                </p>
                            </li>
                            <li>
                                <p>Производится контекстный анализ упоминаний в СМИ, выявляются бумаги, наименее
                                    подверженные критике и имеющие последовательно благоприятное восприятие
                                    инвестиционным
                                    сообществом.</p>
                            </li>
                            <li>
                                <p>Фундаментальный анализ выявляет среди наиболее упоминаемых в позитивном ключе
                                    акций
                                    на рынке США компании следующих типов: демонстрирующие способность к высоким
                                    темпами
                                    органического роста; имеющие стабильный инновационный бизнес и готовящиеся
                                    представить
                                    рынку прорывные технологии (продукты, услуги).</p>
                            </li>

                            <li>
                                <p>Базовый технический анализ на завершающем этапе исследования выявляет
                                    актуальность
                                    идеи, способность к росту, отсутствие перекупленности.</p>
                            </li>
                            <li>
                                <p>После покупки по каждой бумаге отслеживается новостной фон, а также техническая
                                    картина.</p>
                            </li>
                            <li>
                                <p>Закрытию подлежат позиции, демонстрирующие двухзначную или сопровождающуюся
                                    изменением
                                    технической картины на менее благоприятную доходность, либо убыток при негативном
                                    информационном фоне, а также показывающие существенное изменение технической
                                    картины.</p>
                            </li>
                        </ul>
                        <h3>Отчетность</h3>
                        <ul>
                            <li>
                                <p>Клиент ежедневно получает брокерский отчет, в котором отображается цена Ноты на
                                    закрытие
                                    предыдущего торгового дня.
                                </p>
                            </li>
                            <li>
                                <p>Управляющий обязан предоставлять «письмо акционерам» один раз в месяц. Письмо
                                    содержит
                                    информацию о состоянии портфеля, прогнозы на следующий квартал по всем активам,
                                    балансы
                                    всех активов, включая наличные, а также новые инвестиционные идеи на краткосрочную
                                    перспективу.</p>
                            </li>
                            <li>
                                <p>Нота приобретается по цене закрытия в день соответствующей заявки.</p>
                            </li>
                            <li>
                                <p>Нота погашается по цене закрытия в день соответствующей заявки.</p>
                            </li>
                            <li>
                                <p>Все заявки на приобретение или погашение Ноты должны быть поданы за час до закрытия
                                    торговой сессии.</p>
                            </li>
                        </ul>
                        <h3>Параметры Ноты</h3>
                        <table class="table">
                            <tr>
                                <td>Тикер Ноты</td>
                                <th>FN_RST</th>
                            </tr>
                            <tr>
                                <td>Инвестиционная цель стратегии Ноты</td>
                                <th>40% годовых</th>
                            </tr>
                            <tr>
                                <td>Ставка вознаграждения Брокера</td>
                                <th>25% от положительного результата</th>
                            </tr>
                            <tr>
                                <td>Минимальная сумма для подачи заявки на приобретение Ноты</td>
                                <th>10 000 USD</th>
                            </tr>
                            <tr>
                                <td>Комиссия за покупку Ноты</td>
                                <th>1,5%</th>
                            </tr>
                        </table>
                        <p>* – структура на момент формирования Ноты</p>
                    </span>
                </div>
                <div class="buttons">
                    <a href="#" class="button no-active js-note-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                    <a href="#" data-toggle="modal" data-target="#buyModal" class="button">Хочу купить ноту!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="noteActive" class="offers__item" data-amount="100000" data-goal="30">
        <div class="offers__item__header">
            <div class="offers__item__header__line"></div>
            <div class="offers__item__header__img">
                <img src="img/note-bg/active.png" alt="">
            </div>
            <div class="offers__item__header__name">
                <span>Нота</span><br/>
                Freedom<br/>Active
                <a class="pdf-download" href="pdf/active.pdf" download><img src="img/pdf.png" alt=""></a>
            </div>
            <div class="offers__item__header__counts">
                <div class="offers__item__header__counts__item">
                    <p>Минимальная<br/>сумма входа</p>
                    <p class="count">$100 000</p>
                </div>
                <div class="offers__item__header__counts__item">
                    <div class="offers__item__header__dark-line"></div>
                    <p>Целевая<br/>доходность</p>
                    <p class="count">30%</p>
                </div>
            </div>
        </div>
        <div class="offers__item__content">
            <div class="offers__item__content__chart">
                <div class="chart-lines"><img src="img/pie_lines.png" alt=""></div>
                <div id="chartActive"></div>
            </div>
            <div class="offers__item__content__text">
                <div class="text-inner">
                    <h3>Структура*</h3>
                    <ul>
                        <li>
                            <p>Любой актив Ноты может занимать не более 15% портфеля. В случае если изменение в
                                цене
                                повлекло за собой изменение баланса активов и одна из частей стала превышать 15%,
                                управляющий обязан сбалансировать портфель в течение 15 дней с момента возникновения
                                дисбаланса.</p>
                        </li>
                        <span class="mt-hide">
                            <li>
                                <p>Портфель может содержать одновременно не более трех активов из одного сектора.</p>
                            </li>
                            <li>
                                <p>Активный характер Ноты определяет возможность нахождения в ее структуре
                                    значительной
                                    доли денежных средств перед открытием новых позиций.</p>
                            </li>
                        </span>
                    </ul>
                    <span class="mt-hide">
                        <h3>Стратегия</h3>
                        <ul>
                            <li>
                                <p>Управляющий вправе инвестировать в одно IPO в течение 92 дней, а также на свое
                                    усмотрение хеджировать позицию до окончания срока локапа. Размер позиции не может
                                    выходить за рамки структуры Ноты (более 15% всего портфеля).</p>
                            </li>
                            <li>
                                <p>Управляющий вправе использовать кредитные средства (маржу) в размере, не превышающем
                                    50% баланса всего портфеля, для хеджирования или открытия новых позиций перед
                                    закрытием захеджированных позиций.</p>
                            </li>
                            <li>
                                <p>Управляющий вправе использовать опционы для хеджирования позиций (частично или
                                    полностью), а также продавать непокрытые пут-опционы для открытия новых позиций на
                                    момент поставки актива при истечении срока жизни этих опционов. Прогнозируемая
                                    поставка не может превышать пределы структуры Ноты (15% всего портфеля).
                                    Управляющий не вправе покупать непокрытые опционы.</p>
                            </li>
                            <li>
                                <p>Управляющий вправе открывать и держать одну шорт-позицию, не выходя за пределы
                                    структуры Ноты (15% всего портфеля). Этот пункт не учитывает шорты, открытые для
                                    хеджирования позиций IPO в локапе.</p>
                            </li>
                        </ul>
                        <h3>Отчетность</h3>
                        <ul>
                            <li>
                                <p>Клиент ежедневно получает брокерский отчет, в котором отображается цена Ноты на
                                    закрытии предыдущего торгового дня.</p>
                            </li>
                            <li>
                                <p>Управляющий обязан предоставлять «письмо акционерам» один раз в месяц. Письмо
                                    содержит
                                    информацию о состоянии портфеля, прогнозы на следующий квартал по всем активам,
                                    балансы всех активов, включая наличные, а также новые инвестиционные идеи на
                                    краткосрочную перспективу.</p>
                            </li>
                            <li>
                                <p>Нота приобретается по цене закрытия в день соответствующей заявки.</p>
                            </li>
                            <li>
                                <p>Нота погашается по цене закрытия в день соответствующей заявки.</p>
                            </li>
                            <li>
                                <p>Все заявки на приобретение или погашение Ноты должны быть поданы за час до закрытия
                                    торговой сессии.</p>
                            </li>
                        </ul>
                        <h3>Параметры Ноты</h3>
                        <table class="table">
                            <tr>
                                <td>Тикер Ноты</td>
                                <th>FN_ACT</th>
                            </tr>
                            <tr>
                                <td>Инвестиционная цель стратегии Ноты</td>
                                <th>30% годовых</th>
                            </tr>
                            <tr>
                                <td>Ставка вознаграждения Брокера</td>
                                <th>25% от положительного результата</th>
                            </tr>
                            <tr>
                                <td>Минимальная сумма для подачи заявки на приобретение Ноты</td>
                                <th>100 000 USD</th>
                            </tr>
                            <tr>
                                <td>Комиссия за покупку Ноты</td>
                                <th>1,5%</th>
                            </tr>
                        </table>
                        <p>* – структура на момент формирования Ноты</p>
                    </span>
                </div>
                <div class="buttons">
                    <a href="#" class="button no-active js-note-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                    <a href="#" data-toggle="modal" data-target="#buyModal" class="button">Хочу купить ноту!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="notePast" class="offers__item" data-amount="1000" data-goal="30">
        <div class="offers__item__header">
            <div class="offers__item__header__line"></div>
            <div class="offers__item__header__img">
                <img src="img/note-bg/past.png" alt="">
            </div>
            <div class="offers__item__header__name">
                <span>Нота</span><br/>
                Былое<br/>величие
                <a class="pdf-download" href="pdf/past.pdf" download><img src="img/pdf.png" alt=""></a>
            </div>
            <div class="offers__item__header__counts">
                <div class="offers__item__header__counts__item">
                    <p>Минимальная<br/>сумма входа</p>
                    <p class="count">$1000</p>
                </div>
                <div class="offers__item__header__counts__item">
                    <div class="offers__item__header__dark-line"></div>
                    <p>Целевая<br/>доходность</p>
                    <p class="count">30%</p>
                </div>
            </div>
        </div>
        <div class="offers__item__content">
            <div class="offers__item__content__chart">
                <div class="chart-lines"><img src="img/pie_lines.png" alt=""></div>
                <div id="chartPast"></div>
            </div>
            <div class="offers__item__content__text">
                <div class="text-inner">
                    <h3>Описание</h3>
                    <ul>
                        <li>
                            <p>Портфель состоит из компаний электроэнергетического, банковского, технологического и
                                потребительского секторов.</p>
                        </li>
                        <li>
                            <p>В портфель включены известные бренды, имеющие за плечами десятки лет опыта работы на
                                рынке.</p>
                        </li>
                        <span class="mt-hide">
                            <li>
                                <p>В силу разных причин последние несколько лет акции этих компаний переживают
                                    кризисный
                                    период и находятся на исторически низких отметках. Среди основных причин — стагнация
                                    сектора в целом, отсутствие динамики в росте оборота или продаж, заниженные
                                    ожидания
                                    инвесторов.</p>
                            </li>
                            <li>
                                <p>В каждом из отобранных случаев снижение цены акций обосновано, однако, по нашим
                                    оценкам,
                                    указанные компании обладают большим потенциалом роста и могут в обозримом будущем
                                    вернуться на былую высоту.</p>
                            </li>
                        </span>
                    </ul>
                    <span class="mt-hide">
                        <h3>Стратегия</h3>
                        <p>Компания имеет право внести изменения в портфель Ноты в указанных случаях и следующим
                            образом:</p>
                        <ul>
                            <li>
                                <p>В случае если одна из входящих в Ноту бумаг вырастает более чем на 30% в течение 12
                                    месяцев, компания вправе заменить или исключить ее, перераспределив средства между
                                    акциями остальных компаний, входящих в Ноту.</p>
                            </li>
                            <li>
                                <p>В случае исключения бумаги из портфеля взамен приобретается бумага, цена которой на
                                    день
                                    замены находится на отметке не выше 50% от исторического максимума. При этом
                                    компания
                                    должна обладать фундаментальным потенциалом роста.</p>
                            </li>
                            <li>
                                <p>При исключении ценной бумаги должно соблюдаться условие о наличии в портфеле Ноты
                                    акций
                                    не менее четырех эмитентов.</p>
                            </li>
                            <li>
                                <p>Один актив не может занимать более 33,3% портфеля. В случае возникновения дисбаланса
                                    активов компания обязана провести балансировку портфеля Ноты в течение 30 дней.</p>
                            </li>
                        </ul>
                        <h3>Параметры Ноты</h3>
                        <table class="table">
                            <tr>
                                <td>Тикер Ноты</td>
                                <th>FN_BTM</th>
                            </tr>
                            <tr>
                                <td>Инвестиционная цель стратегии Ноты</td>
                                <th>30% годовых</th>
                            </tr>
                            <tr>
                                <td>Ставка вознаграждения Брокера</td>
                                <th>25% от положительного результата</th>
                            </tr>
                            <tr>
                                <td>Минимальная сумма для подачи заявки на приобретение Ноты</td>
                                <th>1000 USD</th>
                            </tr>
                            <tr>
                                <td>Комиссия за покупку Ноты</td>
                                <th>1,5%</th>
                            </tr>
                        </table>
                    </span>
                </div>
                <div class="buttons">
                    <a href="#" class="button no-active js-note-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                    <a href="#" data-toggle="modal" data-target="#buyModal" class="button">Хочу купить ноту!</a>
                </div>
            </div>
        </div>
    </div>

    <div id="noteRetail" class="offers__item" data-amount="1000" data-goal="30">
        <div class="offers__item__header">
            <div class="offers__item__header__line"></div>
            <div class="offers__item__header__img">
                <img src="img/note-bg/retail.png" alt="">
            </div>
            <div class="offers__item__header__name">
                <span>Нота</span><br/>
                Freedom<br/>Retail
                <a class="pdf-download" href="pdf/retail.pdf" download><img src="img/pdf.png" alt=""></a>
            </div>
            <div class="offers__item__header__counts">
                <div class="offers__item__header__counts__item">
                    <p>Минимальная<br/>сумма входа</p>
                    <p class="count">$1000</p>
                </div>
                <div class="offers__item__header__counts__item">
                    <div class="offers__item__header__dark-line"></div>
                    <p>Целевая<br/>доходность</p>
                    <p class="count">30%</p>
                </div>
            </div>
        </div>
        <div class="offers__item__content">
            <div class="offers__item__content__chart">
                <div class="chart-lines"><img src="img/pie_lines.png" alt=""></div>
                <div id="chartRetail"></div>
            </div>
            <div class="offers__item__content__text">
                <div class="text-inner">
                    <h3>Описание</h3>
                    <ul>
                        <li>
                            <p>Портфель Ноты составляется из ценных бумаг представителей сектора Retail, к которым
                                относятся сети розничных магазинов одежды, обуви и аксессуаров.</p>
                        </li>
                        <li>
                            <p>Известные торговые дома в США, владеющие сотнями магазинов на территории страны и за ее
                                пределами, а также популярными узнаваемыми мировыми брендами.</p>
                        </li>
                        <li>
                            <p>В 2015 году сектор розничной торговли достиг своего пика после бурного роста с начала
                                века. Сети открывали новые магазины, расширяли ассортимент, развивали логистику,
                                набирали огромное количество работников, скупали торговые площади и делали ставку на
                                быстрое развитие, не принимая во внимание меняющуюся конъюнктуру.</p>
                        </li>
                        <li>
                            <p>Достигнув своего пика в 2015 году, продажи во всем секторе начали стремительно падать.
                                Бурное развитие индустрии ритейла было прервано онлайн-индустрией. Такие
                                интернет-площадки, как Amazon, начали активно расширять свою долю рынка, предлагая более
                                демократичные цены, обусловленные более низкими расходами на торговые площади,
                                персонал, инвентарь и т.д. Акции ритейлеров упали в разы за два года, что объясняется
                                резким снижением выручки и одновременным ростом долговых обязательств.</p>
                        </li>
                        <li>
                            <p>Анализ показывает, что некоторые компании сектора Retail оцениваются гораздо ниже, чем
                                должны, не только из-за падения всего сегмента, но и из-за большого объема спекулятивных
                                позиций «на понижение» в нем.</p>
                        </li>
                    </ul>
                    <h3>Стратегия</h3>
                    <p>Управляющий имеет право внести изменения в портфель Ноты в указанных случаях и следующим
                        образом:</p>
                    <ul>
                        <li>
                            <p>При сокращении стоимости ценной бумаги на 25% от стоимости приобретения этой бумаги в
                                портфель Ноты или при росте стоимости ценной бумаги на 30% от стоимости приобретения
                                этой бумаги в течение 12 месяцев Управляющий может заменить или исключить эту ценную
                                бумагу.</p>
                        </li>
                        <li>
                            <p>В случае проведения замены выходящей бумаги приобретается ценная бумага из сектора
                                Retail,
                                отвечающая следующим требованиям:
                            <ul>
                                <li>высокий дивиденд или агрессивный выкуп акций</li>
                                <li>генерация годовой прибыли не ниже 5% от капитализации</li>
                                <li>стоимость активов превышает размер долговых обязательств</li>
                            </ul>
                            </p>
                        </li>
                        <li>
                            <p>При исключении ценной бумаги должно соблюдаться условие о наличии в портфеле Ноты не
                                менее
                                четырех ценных бумаг.</p>
                        </li>
                        <li>Один актив не может занимать более 33,3% портфеля. В случае возникновения дисбаланса
                            активов Брокер обязан провести ребалансировку портфеля Ноты в течение 30 дней.
                        </li>
                    </ul>
                    <h3>Параметры Ноты</h3>
                    <table class="table">
                        <tr>
                            <td>Тикер Ноты</td>
                            <th>FN_RTL</th>
                        </tr>
                        <tr>
                            <td>Инвестиционная цель стратегии Ноты</td>
                            <th>40% годовых</th>
                        </tr>
                        <tr>
                            <td>Ставка вознаграждения Брокера</td>
                            <th>25% от положительного результата</th>
                        </tr>
                        <tr>
                            <td>Минимальная сумма для подачи заявки на приобретение Ноты</td>
                            <th>1000 USD</th>
                        </tr>
                        <tr>
                            <td>Комиссия за покупку Ноты</td>
                            <th>1,5%</th>
                        </tr>
                    </table>
                </div>
                <div class="buttons">
                    <a href="#" class="button no-active js-note-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                    <a href="#" data-toggle="modal" data-target="#buyModal" class="button">Хочу купить ноту!</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="about" class="about">
    <img class="about__img" src="img/about.png" alt="">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about__name">
                    <h1>Фридом<br/>Финанс</h1>
                    <p>ведущая инвестиционная компания</p>
                    <p class="more">Узнайте больше на <a href="http://ffin.bz" target="_blank">ffin.bz</a></p>
                </div>
                <div class="about__partners">
                    <img src="img/partners.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="about__list js-about-list">
                    <ul>
                        <li>Компания FFIN Brokerage Services Inc. зарегистрирована в государстве Белиз 23 июля 2014 года
                            под регистрационным номером 147,343 и лицензирована Международной Комиссией по Финансовым
                            Услугам (IFSC).
                        </li>
                        <li>Компания предоставляет международным Клиентам доступ к американским, российским и
                            казахстанским фондовым рынкам и предлагает широкий спектр услуг на финансовых рынках для
                            удовлетворения краткосрочных и долгосрочных финансовых целей своих Клиентов.
                        </li>
                        <li>FFIN Brokerage Services Inc. относится к международной группе компаний Freedom Holding Corp.
                            с листингом на американской бирже OTC, которая регулируется Комиссией по ценным бумагам и
                            биржам США (SEC).
                        </li>
                    </ul>

                    <div class="person__result-list about__result-list js-about-result-list">
                        <div class="person__result-item about__result-item">
                            $115 млн
                            <hr/>
                            <span>собственный капитал Группы</span>
                        </div>
                        <div class="person__result-item about__result-item">
                            $600+ млн
                            <hr/>
                            <span>клиентских активов</span>
                        </div>
                        <div class="person__result-item about__result-item">
                            $14+ млрд
                            <hr/>
                            <span>оборот клиент-сделок,<br/>>97 000 клиентов</span>
                        </div>
                        <div class="person__result-item about__result-item">
                            850+
                            <hr/>
                            <span>профессионалов в 55 офисах по миру</span>
                        </div>
                        <div class="person__result-item about__result-item">
                            35+ IPO
                            <hr/>
                            <span>ключевой провайдер доступа к IPO ведущих компаний мира</span>
                        </div>
                        <div class="person__result-item about__result-item">
                            5 IPO
                            <hr/>
                            <span>на казахстанской бирже KASE и безусловный лидер рынка Казахстана</span>
                        </div>
                    </div>

                    <a href="#" class="button no-active js-about-text-btn"><span>Подробнее</span> <img
                                src="img/arrow.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="person2" class="person">
    <div class="container person__container">
        <div class="row">
            <div class="col-xs-12 col-md-5">
                <div class="person__about">
                    <div class="person__about__line"></div>
                    <img src="img/turlov.png" alt="" id="egor">
                    <h2>Тимур Турлов</h2>
                    <p>Основатель, генеральный директор, глава инвестиционного комитета группы компаний
                        «Фридом Финанс».</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-1">
                <div class="person__text person__text--mt-0 js-person-text-container">
                    <p>В сентябре 2015 году признан лауреатом Всероссийской премии финансистов
                        «Репутация» в номинации «За вклад в развитие фондового рынка стран СНГ».
                    </p>
                    <p>В сентябре 2016 года на
                        Восточном экономическом форуме получил награду за создание инвестиционной системы «Восход». </p>
                    <p>В 2017-м выступил в роли венчурного инвестора, став совладельцем крупнейшего в Казахстане
                        интернет-холдинга Chocofamily.</p>
                    <p>С февраля 2018 года входит в состав Комитета по фондовому рынку
                        Московской биржи.</p>
                    <p>Более 14 лет работает в сфере международных инвестиций. На постоянной основе
                        комментирует финансово-экономическую проблематику в ведущих деловых изданиях России и
                        Казахстана, выступает с лекциями и докладами на международных конференциях, занимается
                        образовательными проектами. </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="contacts" class="contacts">
    <div class="container">
        <h3>Остались вопросы?<br/>Звоните нам —</h3>
        <a class="phone" href="tel:+35722282923">+ 357 22 28 29 23</a>
        <a class="button callback" data-toggle="modal" data-target="#callbackModal" href="#"><img
                    src="img/callback.png"
                    alt=""> Обратный
            звонок</a>
        <p class="address"><a class="email" href="mailto:info@ffin.bz">info@ffin.bz</a>
        </p>

        <!--div class="social">
            <a href="https://vk.com/freedom_finance" target="_blank"><img src="img/vk.png" alt=""></a>
            <a href="https://www.facebook.com/ffinru/" target="_blank"><img src="img/fb.png" alt=""></a>
            <a href="https://www.instagram.com/ffinru/" target="_blank"><img src="img/inst.png" alt=""></a>
        </div-->
    </div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img src="img/logo.png" alt="">
            </div>
            <div class="col-md-7 col-md-offset-2">
                <nav>
                    <ul>
                        <li><a href="#banner">О нотах</a></li>
                        <li><a href="#offers">Предложения по нотам</a></li>
                        <li><a href="#about">О Фридом Финанс</a></li>
                        <li><a href="#contacts">Контакты</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!--p>Компания FFIN Brokerage Services Inc. зарегистрирована в государстве Белиз 23 июля 2014 года под номером
            147,343 и лицензирована Международной Комиссии по Финансовым Услугам (IFSC).</p>
        <p>Компания предоставляет международным Клиентам доступ к американским, российским и казахстанским фондовым
            рынкам и предлагает широкий спектр услуг на финансовых рынках для удовлетворения краткосрочных и
            долгосрочных финансовых целей своих Клиентов.</p>
        <p>
            ООО ИК «Фридом Финанс» оказывает финансовые услуги на территории Российской Федерации в соответствии с
            государственными бессрочными лицензиями на осуществление брокерской, дилерской и депозитарной
            деятельности,
            а также деятельности по управлению ценными бумагами. Государственное регулирование деятельность компании
            и
            защиту интересов ее клиентов осуществляет Федеральная служба по финансовым рынкам России.
        </p>
        <p>
            Владение ценными бумагами и прочими финансовыми инструментами всегда сопряжено с рисками: стоимость
            ценных
            бумаг и прочих финансовых инструментов может как расти, так и падать. Результаты инвестирования в
            прошлом не
            являются гарантией получения доходов в будущем.
            В соответствии с законодательством компания не гарантирует и не обещает в будущем доходности вложений,
            не
            дает гарантии надежности возможных инвестиций и стабильности размеров возможных доходов. Услуги по
            совершению сделок с зарубежными ценными бумагами доступны для лиц, являющихся в соответствии с
            действующим
            законодательством квалифицированными инвесторами и производятся в соответствии с ограничениями,
            установленными действующим законодательством.
        </p-->
        <p class="copy">© FFIN 2018</p>
    </div>
</footer>

<!-- Modals -->
<div id="callbackModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
                <h4 class="modal-title">Заказать обратный звонок</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <input type="hidden" name="text" value="Заявка на обратный звонок">
                    <div class="form-group">
                        <label for="callbackName">Имя</label>
                        <input class="form-control" id="callbackName" type="text" required placeholder="Введите имя">
                    </div>
                    <div class="form-group">
                        <label for="callbackPhone">Телефон</label>
                        <input class="form-control" id="callbackPhone" type="text" required
                               placeholder="Введите телефон">
                    </div>
                    <div class="form-group">
                        <label for="callbackEmail">Email</label>
                        <input class="form-control" id="callbackEmail" type="email" required
                               placeholder="Введите email">
                    </div>
                    <div class="text-center">
                        <button class="button" type="submit">Заказать звонок</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="buyModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
                <h4 class="modal-title">Купить ноту</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <input type="hidden" name="text" value="Заявка на покупку ноты">
                    <div class="form-group">
                        <label for="buyName">Имя</label>
                        <input class="form-control" id="buyName" type="text" required placeholder="Введите имя">
                    </div>
                    <div class="form-group">
                        <label for="buyPhone">Телефон</label>
                        <input class="form-control" id="buyPhone" type="text" required
                               placeholder="Введите телефон">
                    </div>
                    <div class="form-group">
                        <label for="buyEmail">Email</label>
                        <input class="form-control" id="buyEmail" type="email" required placeholder="Введите email">
                    </div>
                    <div class="text-center">
                        <button class="button" type="submit">Купить ноту!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="successModal" class="modal success-modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
                <div class="message">
                    <h3>Ваша заявка отправлена</h3>
                    <p>Спасибо за оставленную заявку. Менеджер свяжется с вами в самое ближайшее время. Удачного
                        дня!</p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($sendSuccess): ?>
    <script>$('#successModal').modal('show');</script>
<?php endif; ?>
<!-- /Modals -->

<!-- Companies Modals -->
<div id="BKE_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/bke.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">The Buckle, Inc.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$1,3 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$0,91 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$89,7 млн</th>
                    </tr>
                    <tr>
                        <td>Дивиденды за 2017 год:</td>
                        <th>4,44%</th>
                    </tr>
                    <tr>
                        <td>Спецдивиденды:</td>
                        <th>10,5</th>
                    </tr>
                    <tr>
                        <td>Beta:</td>
                        <th>0,61</th>
                    </tr>
                    <tr>
                        <td>P/E:</td>
                        <th>14,49</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Сеть магазинов одежды, основанная в 1948 году. На данный момент включает в себя 451 магазин в
                        США и
                        Канаде. Buckle представляет популярные бренды, такие как Obey, BKE, Billabong, Fossil, Roxy,
                        Affliction. Выручка и чистая прибыль падают с 2015-го года, но рост денежного потока
                        восстановился. За счет него компания выплачивает впечатляющие дивиденды. Помимо регулярных
                        дивидендов компания активно выплачивает специальный дивиденд в конце каждого года. За 2017
                        год
                        сумма дивидендов составила 15%.</p>
                    <p>Активы компании: $410 млн. Долгосрочный долг: $0.</p>
                    <p>Не имея долгосрочных долговых обязательств, компания может позволить себе ежегодно выплачивать
                        почти весь свободный денежный поток в виде дивиденда. Компания не владеет торговыми
                        площадями, а
                        значит не обременена выплатами по кредитам на покупку такой недвижимости, которые тянут вниз
                        многих
                        конкурентов по сектору. Кроме того, в непростых условиях рынка аренды коммерческих помещений
                        Buckle фактически имеет возможность диктовать размер арендной платы.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="MRK_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/mrk.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Merck Inc.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$168 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$41 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$1,6 млн</th>
                    </tr>
                    <tr>
                        <td>Активы:</td>
                        <th>$88 млрд</th>
                    </tr>
                    <tr>
                        <td>Долгосрочный долг:</td>
                        <th>$24 млрд</th>
                    </tr>
                    <tr>
                        <td>Дивиденд:</td>
                        <th>3,1%</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Merck & Co., Inc. – международная фармацевтическая и биотехнологическая корпорация. Она была
                        основана в 1891 году, штаб-квартира находится в Кенилворте, штат Нью-Джерси. Компания работает в
                        таких сегментах, как фармацевтика, ветеринария и услуги в сфере здравоохранения, предлагает
                        терапевтические и профилактические средства для лечения широкого спектра заболеваний, включая
                        диабет, инфекционные заболевания, астму и повышенный уровень холестерина в крови. Значительный
                        объем продаж компании на фармацевтическом направлении приходится на вакцины. На сегодня Merck
                        сотрудничает с Pfizer Inc., AstraZeneca PLC, Bayer AG, Eisai Co., Ltd., IO Biotech и Premier
                        Inc.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="CDTX_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/cdtx.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Cidara Therapeutics</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$118 млн</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$0</th>
                    </tr>
                    <tr>
                        <td>Убыток:</td>
                        <th>$59 млн</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Cidara Therapeutics также относится к группе компаний, находящихся на стадии проведения
                        исследований. CDTX специализируется на разработке и коммерциализации новых противоинфекционных
                        средств. В конце мая компания сообщила о начале третьей фазы исследования
                        препарата Rezafungin. Одновременно в CDTX заявили о вторичной эмиссии обыкновенных акций
                        на общую сумму в $120 млн. Заметная активность ведущих фондов, специализирующихся на
                        биотехнологиях, безусловно, повлияет на рост цены акции в ближайшее время: Biotechnology
                        Value Fund объявил о 9,99% стейке в Cidara Therapeutics 4 июня, в этот же день Opaleye
                        Management объявил 7,81% стейк. Большинство тренд-аналитиков рекомендуют акции компании к
                        покупке, ориентируясь на перспективы разработок компании.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="GILD_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/gild.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Gilead Sciences</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$93 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$25 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$14 млрд</th>
                    </tr>
                    <tr>
                        <td>Активы:</td>
                        <th>$70 млрд</th>
                    </tr>
                    <tr>
                        <td>Долгосрочный долг:</td>
                        <th>$31 млрд</th>
                    </tr>
                    <tr>
                        <td>Дивиденд:</td>
                        <th>3,5%</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Компания Gilead Sciences — это один из самых известных биотехов. Изначально разрабатывала и
                        продавала вакцины против ВИЧ, что приносило ей стабильную прибыль. В 2011 году менеджмент
                        принял рискованное решение купить за $11 млрд компанию Pharmasset, которая разрабатывала
                        препарат под названием Sovaldi, полностью излечивающий гепатит С. На тот момент препарат
                        проходил третью фазу исследований, но уже показал впечатляющие результаты в двух первых. В
                        завершающей фазе применение Sovaldi обеспечивало излечение от гепатита C в 91% случаев. К 2015
                        году Sovaldi приносил компании более $15 млрд прибыли. Однако победа над гепатитом C обернулась
                        против компании, выручка и доход которой от Sovaldi начали падать.
                    </p>
                    <p>Инвесторы долгое время ожидали от руководства компании покупки сторонних проектов с
                        перспективными разработками, которые впоследствии обеспечат рост прибыли. В августе 2017 года
                        Gilead купила Kite Pharma, которая известна многообещающими разработками в сфере онкологии. Если
                        препарат Kite Pharma пройдет испытания и покажет положительный результат, это принесет
                        миллиардные доходы Gilead Sciences.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="ICPT_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/icpt.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Intercept Pharmaceuticals</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$2 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$130 млн</th>
                    </tr>
                    <tr>
                        <td>Убыток:</td>
                        <th>$360 млн</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Intercept Pharmaceuticals относится к компаниям, чьи перспективные препараты сейчас находятся на
                        стадии разработки. Несмотря на то, что у компании есть один продукт, успешно прошедший
                        испытания, наибольшие ожидания прибыли возлагаются на препарат против NASH (ожирение печени) —
                        заболевания, которое на сегодняшний день не лечится. Потенциальный рынок этого лекарства
                        оценивается в десятки миллиардов долларов. Компания планирует завершить третью фазу
                        исследований к концу 2019 года. По разным оценкам, она входит в число лидеров среди
                        разработчиков лекарства от NASH.
                    </p>
                    <p>В июне 2018-го года появились слухи о том, что некоторые крупные игроки биотехнологического
                        сегмента рассматривают возможность купить ICPT целиком, что говорит о высоком потенциале этой
                        компании еще до официального оглашения результатов третьей фазы исследований упомянутого
                        препарата.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="MYOK_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/myok.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">MyoKardia Inc.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$2 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$25 млн</th>
                    </tr>
                    <tr>
                        <td>Убыток:</td>
                        <th>$49 млн</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Как и Intercept Pharmaceuticals, MyoKardia Inc. относится к компаниям, перспективные препараты
                        которых находятся на стадии разработки и не представлены на рынке. В 2017 году компания объявила
                        о положительных результатах второй фазы разработок их передового препарата для пациентов с
                        гипертрофической кардиомиопатией (утолщение стенки левого желудочка сердца).
                    </p>
                    <p>На сегодняшний день препаратов такого класса не существует, а потенциальный рынок оценивается в
                        более чем $5 млрд в год. MyoKardia Inc. разрабатывает и другие лекарства для борьбы с
                        кардиологическими заболеваниями. Результаты третьей фазы ожидаются во второй половине 2020
                        года. На завершающей фазе компании, как правило, публикуют промежуточные результаты и
                        новости, которые могут позитивно сказаться на стоимости акций.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="WELL_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/well.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Welltower Inc.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$21,38 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка за 2017 год:</td>
                        <th>$4,2 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$485,53 млн</th>
                    </tr>
                    <tr>
                        <td>Дивиденды за 2017 год:</td>
                        <th>6,45%</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Beta:</td>
                        <th>0,45</th>
                    </tr>
                    <tr>
                        <td>P/E:</td>
                        <th>36,28</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Welltower – это REIT (Real Estate Investment Trust – инвестиционный траст недвижимости),
                        который специализируется на учреждениях, связанных с сектором здравоохранения. Welltower —
                        самый крупный REIT в своем секторе, выручка которого на конец 2017 года составляла $4,3
                        млрд. На протяжении последних девяти месяцев акции падали в цене на 23,6%. Однако в мае
                        ситуация изменилась, и технически происходит отскок, а котировка пробила уровень сопротивления
                        $55,72.</p>
                    <p>С позиций фундаментального анализа, инвестиционный траст обладает высоким потенциалом роста,
                        поскольку население США стареет, и в ближайшие два-три года здесь будет увеличиваться спрос на
                        дома престарелых и другие медицинские учреждения. В ожидании этого инвесторы начнут скупать
                        акции WELL уже в ближайшее время.</p>
                    <p>Welltower выплачивает высокие дивиденды в размере более 6%, что обуславливает привлекательность
                        компании для долгосрочных вложений от одного года.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="VGR_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/vgr.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Vector Group Ltd.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$2,68 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка за 2017 год:</td>
                        <th>$1,8 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$84,57 млн</th>
                    </tr>
                    <tr>
                        <td>Дивиденды за 2017 год:</td>
                        <th>8,10%</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Beta:</td>
                        <th>0,40</th>
                    </tr>
                    <tr>
                        <td>P/E:</td>
                        <th>33,76</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Холдинговая компания, основанная в 1911 году. Основное направление бизнеса – производство и
                        продажа табачной продукции под марками EVE, Pyramid, Grand Prix, Liggett Select, и Eagle
                        20’s.</p>
                    <p>Второе по значимости направление деятельности – строительство и продажа жилых домов компанией
                        New Valley LLC. Вклад этого сегмента холдинга в общую долю прибыли в последнее время составляет
                        порядка 10%. Обеспечивает годовую дивидендную доходность свыше 8%.</p>
                    <p>Акции торгуются в узком диапазоне $19–23.</p>
                    <p>Ежеквартально компания выплачивает $0,4 дивиденда на акцию с 2013 года ($1,6 в год). Vector Group
                        Ltd. Демонстрирует стабильный рост выручки и прибыли последние три года, что служит залогом
                        позитивных оценок ее потенциала.</p>
                    <p>Прогнозируем сохранение дивидендной политики на фоне отсутствия значительных ухудшений
                        финансовых показателей.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="IRT_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/irt.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Independence Realty Trust, Inc.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$871,7 млн</th>
                    </tr>
                    <tr>
                        <td>Выручка за 2017 год:</td>
                        <th>$161,21 млн</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$30,206</th>
                    </tr>
                    <tr>
                        <td>Дивиденды за 2017 год:</td>
                        <th>7,74%</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Beta:</td>
                        <th>1,18</th>
                    </tr>
                    <tr>
                        <td>P/E:</td>
                        <th>24,29</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Самоуправляемый инвестиционный траст недвижимости. Владеет недвижимостью, управляет ею, сдает
                        в аренду, инвестирует в строительство и готовые активы. Владеет и управляет более 15 тыс.
                        объектов недвижимости в США.</p>
                    <p>Компания ежегодно выплачивает дивиденды, при этом она не снижала объем выплат в денежном
                        исчислении в течение последних пяти лет. На данный момент нет оснований полагать, что
                        менеджмент планирует изменить дивидендную политику.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="MBT_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/mbt.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Mobile TeleSystems</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$9,05 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка за 2017 год:</td>
                        <th>$7,6 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$1,01 млрд</th>
                    </tr>
                    <tr>
                        <td>Дивиденды за 2017 год:</td>
                        <th>9,03%</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Beta:</td>
                        <th>0,86</th>
                    </tr>
                    <tr>
                        <td>P/E:</td>
                        <th>9,00</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Российская телекоммуникационная компания, оказывающая услуги связи в России и странах СНГ под
                        торговой маркой МТС. Крупнейший мобильный оператор в РФ и на постсоветском
                        пространстве.</p>
                    <p>48,1% компании принадлежит АФК Система, которая по решению суда должна выплатить крупную сумму
                        Роснефти. В данном случае необходимость получения дополнительных средств от МТС будет играть на
                        руку акционерам, поскольку Система вынуждена будет пойти на повышение дивидендных выплат.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="GPS_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/gps.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">GAP Inc.</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$12 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$15,85 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$848 млн</th>
                    </tr>
                    <tr>
                        <td>Активы:</td>
                        <th>$8 млрд</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Долгосрочный долг:</td>
                        <th>$1,25 млрд</th>
                    </tr>
                    <tr>
                        <td>Дивиденд:</td>
                        <th>3%</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Основанная в 1969 году GAP и ее дочерние бренды (Old Navy, Gap, Banana Republic и Athleta) одни
                        из самых узнаваемых в мире. После публикации последнего квартального отчета акции компании упали
                        на 15%, несмотря на относительно неплохие результаты. Негативная динамика котировок объясняется
                        завышенными ожиданиями инвесторов в отношении отчетности.</p>
                    <p>Тем не менее выручка и чистый доход торговой сети продолжают расти в сравнении с прошлым годом.
                        Мы рассчитываем, что акции продолжат двигаться вверх в ближайшее время, полагаясь на сильные
                        фундаментальные факторы. P/E у Gap Inc. один из самых низких в секторе ритейла, а объем
                        наличности на балансе превышает размер всего долгосрочного долга.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="DDS_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/dds.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Dillard’s</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$2,6 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$6,4 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$221 млн</th>
                    </tr>
                    <tr>
                        <td>Активы:</td>
                        <th>$3 млрд</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Долгосрочный долг:</td>
                        <th>$730 млн</th>
                    </tr>
                    <tr>
                        <td>Дивиденд:</td>
                        <th>0,54%</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Торговая сеть основана в 1938 году. Ее выручка и чистая прибыль падают с 2015 года. Компания
                        выплачивает низкий дивиденд, но компенсирует это агрессивным выкупом собственных акций. В
                        начале 2018 года Dillard’s объявила о программе выкупа акций на $500 млн, что на тот момент
                        составляло почти 25% всех бумаг. В настоящее время доля акций основателей компании составляет
                        44%, и есть основания полагать, что они планируют выкупить компанию целиком в течение
                        нескольких лет. Такое решение основателей Dillard’s связано с оценкой всего бизнеса
                        компании ниже рыночной цены всей ее недвижимости.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="AEO_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/aeo.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">American Eagle Outfitters</h4>
                <table class="table">
                    <tr>
                        <td>Капитализация:</td>
                        <th>$4,3 млрд</th>
                    </tr>
                    <tr>
                        <td>Выручка:</td>
                        <th>$3,8 млрд</th>
                    </tr>
                    <tr>
                        <td>Прибыль:</td>
                        <th>$204 млн</th>
                    </tr>
                    <tr>
                        <td>Активы:</td>
                        <th>$1,8 млрд</th>
                    </tr>
                    <tr>
                    <tr>
                        <td>Долгосрочный долг:</td>
                        <th>$0</th>
                    </tr>
                    <tr>
                        <td>Дивиденд:</td>
                        <th>2,5%</th>
                    </tr>
                </table>
                <div class="text">
                    <p>Компания основана в 1977 году. История American Eagle Outfitters довольно проста и понятна
                        каждому инвестору. Ее характеризует рост выручки и прибыли при полном отсутствии долгосрочных
                        долговых обязательств. AEO, пожалуй, единственная сеть розничных магазинов, сумевшая увеличить
                        оборот в течение последних трех лет. Компания получает существенную долю выручки от
                        онлайн-продаж, что уменьшает ее уязвимость в конкуренции с Amazon и другими
                        онлайн-гигантами.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="GE_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/ge.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">General Electric</h4>
                <div class="text">
                    <p>Одна из старейших и самых известных компаний в мире. GE представляет собой многопрофильный
                        конгломерат, связанный с большим количеством продуктов и сервисов: от стиральных машин и
                        турбодвигателей пассажирских авиалайнеров до банковской и страховой деятельности. Бренд
                        General Electric стоит наравне с General Motors, Ford, Boeing, Coca-Cola. В честь компании даже
                        назван город в США, где находится ее штаб-квартира.</p>
                    <p>К сожалению, бурный рост GE в начале XXI века, а также несколько процессов слияний и
                        поглощений негативно сказались на балансе компании, которая накопила $125 млрд долга. GE
                        необходима реструктуризация, кроме того, стоит задача продажи некоторых активов и снижения
                        объема долга. Руководство уже работает над реализацией этих целей. Так, в мае 2018 года
                        компания продала свой железнодорожный бизнес, что свидетельствует о намерениях GE провести
                        реструктуризацию и улучшить финансовую ситуацию. Безусловно, это непростая задача, однако, по
                        нашим оценкам, выполнимая.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="DB_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/db.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Deutsche Bank</h4>
                <div class="text">
                    <p>Пожалуй, самый известный финансовый институт в Европе и крупнейший банк Германии. Несмотря
                        на то, что активы DB превышают €1,5 трлн, они далеко не самые ликвидные и перспективные, что
                        негативно сказывается на восприятии потенциальных инвесторов. Так, резкое падение стоимости
                        облигаций Италии в конце мая, часть которых принадлежат DB, привело к падению акций компании
                        на 3% за один день, вновь опустив цену бумаг банка на исторический минимум.</p>
                    <p>Предыдущее резкое снижение цены бумаг DB произошло в 2015 году на фоне коррекции рынка, обвала
                        нефти и колоссального укрепления доллара. При этом за прошедшее время банк переживал и периоды
                        активного роста: акции дорожали в два раза на фоне позитивных новостей и предложений помощи со
                        стороны правительства Германии. Именно поэтому мы считаем, что DB too big to fail и нынешнее
                        двойное дно – это идеальная возможность купить его акции.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="JCP_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/jcp.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">JC Penney</h4>
                <div class="text">
                    <p>Одна из самых старых сетей розничной торговли, продающая одежду, обувь, аксессуары. В сети 875
                        магазинов, 98000 работников. Стоимость ее активов — $8,5 млрд, выручка — $12,5 млрд в год.
                        Однако при таких показателях низкая капитализация – всего $800 млн. Впрочем, это вполне
                        объяснимо. Онлайн-магазины оказывают негативное влияние на офлайновый ритейл,
                        специализирующийся на одежде. Весь сектор в среднем потерял порядка 32% выручки с 2015 года,
                        когда на рынок вышли такие онлайн-гиганты, как Amazon.</p>
                    <p>До 2015 года JCP переживала период бурного развития, увеличивая количество магазинов и персонала,
                        для этого компания взяла на себя большой объем долговых обязательств, достигший $4,2 млрд. Мы
                        считаем, что именно у JCP самый мощный потенциал восстановления. Количество активов
                        существенно превышает обязательства, при этом идет постепенное восстановление сектора ритейла в
                        целом.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="KHC_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/khc.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Kraft Heinz Co</h4>
                <div class="text">
                    <p>Kraft Heinz прежде всего ассоциируется с кетчупом, поскольку именно она является крупнейшим
                        производителем этого продукта в мире. Кроме томатного соуса KHC предлагает широкий ассортимент
                        продуктов питания, среди которых макаронные изделия, напитки, полуфабрикаты и многое другое.
                        Компания захватила значительную долю рынка и продолжает доминировать в своем сегменте, однако на
                        цене акций монопольное положение KHC не отражается: она сейчас находится практически на
                        исторических минимумах.</p>
                    <p>Ключевая проблема Kraft Heinz – полное отсутствие новых идей, а также возможностей поглощений
                        или слияний. Инвесторы больше заинтересованы в компаниях, которые постоянно динамично
                        развиваются, открывают для себя новые горизонты, ищут перспективные проекты и поглощают успешных
                        конкурентов.
                        Мы считаем, что нынешние условия на рынке благоприятны для покупки такого актива, как KHC:
                        компания по-прежнему выплачивает дивиденды и способна реализовать достаточный потенциал роста
                        при правильном подходе менеджмента. В случае удачных управленческих решений мы увидим
                        позитивную динамику ее показателей, в том числе и увеличение инвестиционной
                        привлекательности.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="SNAP_Modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <img class="logo" src="img/companies/snap.jpg" alt="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="img/close.png" alt="">
                </button>
            </div>
            <div class="modal-body">
                <h4 class="modal-title">Snap Inc.</h4>
                <div class="text">
                    <p>Самая молодая компания в нашем списке. Социальная платформа SnapChat лидирует по популярности в
                        молодежной среде. Компания вышла на IPO всего два года назад. Ее акции взлетели в цене почти
                        вдвое за первые три дня торгов, однако быстро скорректировались в связи с изначальной
                        переоценкой стоимости. В течение года котировки упали с $28 до $11 за бумагу.</p>
                    <p>Однако, по нашим прогнозам, компания имеет высокий потенциал роста, поскольку способна
                        адаптироваться к рынку и хорошо ориентируется в запросах молодежной аудитории. Последний
                        фактор напрямую связан с тем, что практически весь менеджмент состоит из представителей
                        молодого поколения, понимает потребности сверстников и даже задает тренды в социальных медиа.
                        Одновременно в акциях накопилось значительное количество шортов (позиций на продажу). Закрытие
                        этих позиций вкупе с улучшением результатов компании будет способствовать устойчивому
                        повышению цены ее акций.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Companies Modals -->

<script src="js/canvasjs.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>